# Learning Algorithms and Competetive Programming
[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)
<br><br>
The repository is a collection of a variety of algorithms implemented in C++ along with solutions to problems practiced to improve competetive programming.
